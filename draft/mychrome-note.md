Chrome 扩展
=============

## 开发者工具

1. Wappalyzer 分析网站使用的css和js库
2. WordPress Theme Detector and Plugin Detector 分析基于wordpress建的网站使用的主题和插件

## 其它工具

1. Tampermonkey
2. Gooreplacer 替换访问网站的google cdn jquery为国内的cdn链接，加快访问网站
3. floccus bookmarks sync  标签同步工具
4. SwitchyOmega

