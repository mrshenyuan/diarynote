Node sass 

node-sass/v4.12.0/win32-x64-83_binding.node

# 1. 下载对应版本到本地磁盘

[https://github.com/sass/node-sass/releases/](https://github.com/sass/node-sass/releases/)

如下载的文件放入的路径是: `D:\tools\nodejs\win32-x64-83_binding.node`


# 2. 设置sass路径


```
#npm config set SASS_BINARY_PATH=本地磁盘路径

npm config set SASS_BINARY_PATH=D:\tools\nodejs\win32-x64-83_binding.node
```
 

# 3.安装


npm i node-sass -D –verbose

## References

* [解决 win32-x64-64_binding.node 下载失败](https://www.cnblogs.com/gispathfinder/p/12996464.html)